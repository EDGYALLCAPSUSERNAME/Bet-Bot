BetBot
======
What is BetBot?
----------------
This is the source code for [/u/ARBetBot](www.reddit.com/u/ARBetBot).

BetBot is a reddit bot, that can be seen in action over at
[/r/betonaskreddit](www.reddit.com/r/betonaskreddit). BetBot works by pulling
rising threads from a chosen subreddit and posting Bet threads in a betting
subreddit. BetBot then handles collecting the bets, calculating winnings, and
posting the results. All the data used by the bot is stored in a sqlite database,
which is managed in the code with [Dataset](https://dataset.readthedocs.org/en/latest/index.html).

BetBot is designed so it can be used by anyone, with any two subreddits of there
choice.

Setup
-----
Included in the repo is a config_bot_skeleton.py file, this is used to setup the
credentials and user agent for your bot, after adding the credentials rename
the file to config_bot.py.

Also included is a message_template.py file, which contains every message, comment,
or submission text the bot will post. Feel free to edit these to your liking.
Keep in mind, if you edit them too much if they are format strings, you will have
to change the corresponding _post() method in BetBot.py so the correct data gets
formatted in the string.

Then you just setup the run_bet_bot.py and run_bet_bot_comments.py scripts. You
will need a praw-multiprocess running so it can scroll comments and handle gathering
bets.

To start the bot you initialize a BetBot with:

    bet_checking = BetBot(dataset database object,
                          praw reddit object,
                          "betting_subreddit",
                          "pull_subreddit",
                          REDDIT_USERNAME,
                          REDDIT_PASS,
                          REDDIT_USERAGENT,
                          Debug = True/False)

If you decide to use BetBot for you own subreddit, let me know! I would love to
check it out. Just PM me on Reddit.

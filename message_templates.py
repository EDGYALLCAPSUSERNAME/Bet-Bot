OPEN_POST_TITLE = "[Betting Open] {}"

OPEN_THREAD_BODY = """
Betting is now open on **{}**\n\n It will be open for the next 6 hours.
You may bet as much as you want, if you have that much in the bank. After 6 hours, a betting closed thread
will be created, displaying the top 5 winners. The imaginary
money will be placed in your name after the 6 hours depending how much
you won or lost.\n
You can post a comment saying \"BetBot! Bank\" and you will recieve a
reply saying how much you currently have.\n
In [Betting Open] threads, you can post \"BetBot! Bet *amount* *answer*\" and
and amount in that will be placed. You cannot change you bet after it's
been placed. And you can only bet once.\n
[You can view the AskReddit thread here.](http://np.reddit.com/r/AskReddit/comments/{})\n
"""

OPEN_THREAD_ODDS = """
**Current Leaders**\n
|Place|Comment|Score|Change|Updated|
:----:|:-----:|:----|:----:|-------:
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
\n
"""

RESULTS_THREAD_BODY = """
The results of **{}** are in! \n\n
|Top Comment|Results|
:-----------:|:-----:
|The comment was|{}|
|Comment score|{}|
|Percentage of players who bet this|{}|
\n
**First Place Winners Are:**\n
|Winner|Amount Won|Bet|
:-----:|:--------:|---:
{}\n
|Second Highest Comment|Results|
:-----------:|:-----:
|The comment was|{}|
|Comment score|{}|
|Percentage of players who bet this|{}|
\n
**Second place winner are:**\n
|Winner|Amount Won|Bet|
:-----:|:--------:|---:
{}\n
|Third Highest Comment|Results|
:-----------:|:-----:
|The comment was|{}|
|Comment score|{}|
|Percentage of players who bet this|{}|
\n
**Third place winners are:**\n
|Winner|Amount Won|Bet|
:-----:|:--------:|---:
{}\n
|Fourth Highest Comment|Results|
:-----------:|:-----:
|The comment was|{}|
|Comment score|{}|
|Percentage of players who bet this|{}|
\n
**Fourth place winner ares:**\n
|Winner|Amount Won|Bet|
:-----:|:--------:|---:
{}\n
|Fifth Highest Comment|Results|
:-----------:|:-----:
|The comment was|{}|
|Comment score|{}|
|Percentage of players who bet this|{}|
\n
**Fifth place winners are:**\n
|Winner|Amount Won|Bet|
:-----:|:--------:|---:
{}\n
\n
Congrats to the winners!\n
[You can view the AskReddit thread here.](http://np.reddit.com/r/AskReddit/comments/{})\n
[You can view the betting thread here.](http://www.reddit.com/r/BetOnAskReddit/comments/{})
"""

RESULTS_POST_TITLE = "[Results] {}"

REGISTERED_REPLY = """
Congratulations, you are now registered and can start betting.\n
You'll start off with a free 1000CJ!\n
Have fun gambling it away!
"""

GET_REGISTERED_REPLY = """
You need to get registered before you can bet!\n
[Go here to get registered and read about how this whole thing works and then come back and make another comment to bet.](http://www.reddit.com/r/betonaskreddit/comments/37mz6a/welcome_to_bet_on_ask_reddit_read_this_to_get/)
"""

TOO_MUCH_REPLY = """
Sorry I couldn't make this bet for you, because you're too poor.\n
You only have {}. Try posting another comment betting less you degernerate.
"""

BET_MADE_REPLY = """
I got your bet in, be looking in your pms to see if you've won!
"""

BET_TWICE_REPLY = """
I guess you can't read, since the rules say you can only bet once on a thread.\n
This one doesn't count.
"""

BANK_REPLY = """
You currently have {}, happy gambling!
"""
WINNER_MESSAGE = """
You're a big winner. You bet on {} and won some fake points!\n
You won exactly {} circle jerk points. Keep coming back!\n
[Check out the results thread here.](http://www.reddit.com/r/betonaskreddit/{})
"""

FORGOT_COMMAND_REPLY = """
Hey, I don't know what you want me to do with this! If you edit your comment with a command after you summon me, I'll do the right thing. I promise!
"""

CLOSED_THREAD_REPLY = """
Hey, sorry buddy but this thread is closed for betting. [You can still check out the currently open one's though.](http://op.reddit.com/r/betonaskreddit/#op).
"""

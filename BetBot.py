#-------------------------------------------------------------------------------
# ARBot.py
# Version 0.5
#-------------------------------------------------------------------------------

import praw
import re
import time
import datetime
import dataset
import sqlalchemy
import urllib2
from difflib import SequenceMatcher
from message_templates import *

#-------------------------------------------------------------------------------

class BetBot:

    ''' Class for functions of the BetBot
        as seen on reddit/r/betonaskreddit
    '''

    def __init__(self,
                 db,
                 r,
                 bet_sub,
                 pull_sub,
                 username,
                 password,
                 user_agent,
                 debug=False):

        '''
            pre: db-database, r-praw reddit object
            post: database, reddit object, as well as subbreddit vars are
                  initialzed.
        '''
        self.db = db
        self.users = self._createTable('users')
        self.threads = self._createTable('threads')
        self.replied_to = self._createTable('replied_to')
        self.no_command_replied = self._createTable('no_command_replied')
        self.r = r
        self.username = username
        self.password = password
        self.bet_sub = self.r.get_subreddit(bet_sub)
        self.pull_sub = self.r.get_subreddit(pull_sub)
        self.debug=debug

        users_table_columns = {'username': sqlalchemy.String,
                                    'total_money': sqlalchemy.Float}
        self._createColumns(self.users, users_table_columns)

        threads_table_columns = {'thread_title': sqlalchemy.String,
                                 'bet_thread_id': sqlalchemy.String,
                                 'pull_thread_id': sqlalchemy.String,
                                 'time_opened': sqlalchemy.Float,
                                 'time_closed': sqlalchemy.Float,
                                 'is_thread_open': sqlalchemy.Boolean,
                                 'date_posted': sqlalchemy.DateTime}
        self._createColumns(self.threads, threads_table_columns)

        replied_to_table_columns = {'comment_id': sqlalchemy.String,
                                    'date_replied': sqlalchemy.DateTime}
        self._createColumns(self.replied_to, replied_to_table_columns)

        no_command_table_columns = {'comment_id': sqlalchemy.String,
                                    'date_replied': sqlalchemy.DateTime}
        self._createColumns(self.no_command_replied, no_command_table_columns)

        self.bet_table_columns = {'user': sqlalchemy.BigInteger,
                                  'bet_amount': sqlalchemy.Float,
                                  'bet_string': sqlalchemy.String,
                                  'time_of_bet': sqlalchemy.Float,
                                  'bet_thread_id': sqlalchemy.String}

        self.odds_table_columns = {'comment': sqlalchemy.String,
                                   'comment_id': sqlalchemy.String,
                                   'score': sqlalchemy.Integer,
                                   'delta': sqlalchemy.Integer,
                                   'updated': sqlalchemy.String }

#-------------------------------------------------------------------------------

    def _createTable(self, tablename):

        '''
            pre: table name to create
            post: creates or loads table
        '''

        return self.db.get_table(tablename)


#-------------------------------------------------------------------------------

    def _createColumns(self, table, columns):

        '''
            Creates columns for the tables:
            pre: a table object and a columns dictionary
                 dictionary must be in format of:
                 \{column_name: sqlalchemy.datatype\}
            post: columns in table are created.
        '''

        for c_name, c_type in columns.iteritems():
            table.create_column(c_name, c_type)

#-------------------------------------------------------------------------------

    def start(self, job):

        '''
            pre: none
            post: starts the BetBot
        '''

        print "Logging in..."
        self._login()
        print "Starting bet crawling..."
        if job == "bets":
            self._bettingLoop()
        elif job == "comments":
            self._commentsLoop()
        else:
            while True:
                print "Calculating odds..."
                self._calculateOdds()
                print "Sleeping..."
                time.sleep(1800)
#-------------------------------------------------------------------------------

    def _login(self):

        '''
            pre: none
            post: logs in to reddit
        '''

        self.r.login(self.username, self.password)

#-------------------------------------------------------------------------------

    def _commentsLoop(self):

        ''' pre: none
            post: fetchs any new users, and checks for BetBot! comments
        '''

        while True:
            print "Fetching New Users..."
            self._fetchNewUsers()

            self._checkForBetBotComments()

#-------------------------------------------------------------------------------

    def _bettingLoop(self):

        '''
            pre: none
            post: runs the main loop of the bot
        '''

        t = time.time()

        while True:
            try:
                # Only open threads once every 2 hours
                if (t + (3600 * 2)) < time.time():
                    print "Opening new threads..."
                    self._openThreads()
                    t = time.time()

                print "Closing threads..."
                self._closeThreads()

                print "Gathering results..."
                self._getResults()

                print "Checking for broke users..."
                self._checkIfUserIsBroke()

            except urllib2.HTTPError, e:
                if e.code in [429, 500, 502, 503, 504]:
                    print "Reddit is down (Error: {}), sleeping...".format(e.code)
                    time.sleep(60)
                    pass
                else:
                    raise
            except Exception, e:
                print "couldn't Reddit: {}".format(str(e))
                raise
    #-------------------------------------------------------------------------------

    def _fetchNewUsers(self):

        '''
            pre: none
            post: table users in database is modifed to any new users if
                  found.
        '''
        new_users_sticky = self.r.get_submission(submission_id='37mz6a')
        flat_comments = praw.helpers.flatten_tree(new_users_sticky.comments)
        for comment in flat_comments:
            if self.users.find_one(username = str(comment.author)) == None:
                new_user = self._parseObject('comment', comment)
                self.users.insert(dict(username=new_user[0],
                                       total_money=1000))
                self._post('registered', comment)

#-------------------------------------------------------------------------------

    def _openThreads(self):

        '''
            pre: none
            post: new threads are created pulled from pull subreddit and added
                  to the table threads a post is made for them by calling
                  _post()
        '''

        max_open = 0
        for submission in self.pull_sub.get_rising(limit = 1000):
            if submission.score > 15 and max_open < 6:
                # Ignore serious threads, because those aren't circlejerky enough
                if not re.search("serious", str(submission), re.IGNORECASE):
                    # Check to make sure it hasn't already been posted
                    if self.threads.find_one(pull_thread_id = str(submission.id)) == None:
                        pull_thread_info = self._parseObject('submission', submission)
                        open_bet_post = self._post('open', pull_thread_info)
                        self.threads.insert(dict(thread_title = open_bet_post[0],
                                                 bet_thread_id = open_bet_post[1],
                                                 pull_thread_id = pull_thread_info[1],
                                                 time_opened = float(time.time()),
                                                 is_thread_open = True,
                                                 date_posted = datetime.datetime.utcnow()))
                        subm = self.r.get_submission(submission_id = open_bet_post[1])
                        subm.set_flair(flair_css_class = 'open',
                                       flair_text = 'Betting Open')
                        max_open += 1


#-------------------------------------------------------------------------------

    def _closeThreads(self):

        '''
            pre: none
            post: if a thread is open for more than 6 hours the threads table
                  is modifed to set it as closed, and adds a time closed stamp.
        '''

        open_threads = self.threads.find(is_thread_open = True)
        for thread in open_threads:
            if (thread['time_opened'] + float(21600)) < time.time():
                thread_id = thread['id']
                self.threads.update(dict(id = thread_id,
                                         is_thread_open = False,
                                         time_closed = time.time()),
                                         ['id'])

                closed_thread = self.r.get_submission(submission_id = thread['bet_thread_id'])
                closed_thread.set_flair(flair_css_class = 'closed',
                                        flair_text = 'Betting Closed')

#-------------------------------------------------------------------------------

    def _gatherBets(self):

        '''
            pre: none
            post: all bets from open threads are gathered and sent to _makeBet
        '''

        open_threads = self.threads.find(is_thread_open = True)
        for thread in open_threads:
            # get the praw submission object
            submission = self.r.get_submission(submission_id = thread['bet_thread_id'])
            flat_comments = praw.helpers.flatten_tree(submission.comments)
            for comment in flat_comments:
                # Check if bot has already replied to user
                if not self._checkIfBotAlreadyReplied(comment):
                    # Check if bot was summoned
                    if re.search("BetBot! Bet", str(comment), re.IGNORECASE):
                        print "Found one!"
                        comment_info = self._parseObject('comment', comment)
                        # If user is not registered, tell them to get registered
                        if not self._checkIfRegistered(comment_info[0]):
                            comment_info.append(comment)
                            self._post('get_registered', comment_info[0])
                        # Make the bet
                        else:
                            bet_list = str(comment).split(" ")
                            self._makeBet(comment, comment_info, bet_list[2:], time.time(), thread)

#-------------------------------------------------------------------------------

    def _makeBet(self, comment, comment_info, bet_list, time, thread):

        '''
            pre: username, a list of bet information, and database row
                 of corresponding thread.
            post: a table of all bets made on thread is created
        '''

        # The naming here sucks, and I'm sorry to anyone that reads this.
        # Basically the 'thread' that got passed in as a param is a
        # praw submission object, and not a threads database entry.
        # So all this line does is switch it from being a praw submission obj
        # to a db entry.
        thread = self.threads.find_one(bet_thread_id = thread.id)

        if re.search("AllIn", bet_list[0], re.IGNORECASE):
            user = self.users.find_one(username = comment_info[0])
            bet_amount = user['total_money']
        else:
            # test to make sure bet amount can be convereted
            try:
                float(bet_list[0])
                bet_amount = float(bet_list[0])
            except ValueError:
                print "Value Error on Bet post... letting user know"
                self._post('error', comment)
                return False

        bet_string = " ".join(bet_list[1:])
        username = comment_info[0]
        # append the comment object so the bot can reply to it
        comment_info.append(comment)
        # Loads table if it currently exists, or creates it if it does not
        # Table is named after the betting thread id
        bet_table = self.db.get_table(thread['bet_thread_id'])
        self._createColumns(bet_table, self.bet_table_columns)

        # Checks for the conditions if they user doesn't have enough money
        # Then checks if they've already bet on this threads, and then will
        # place the bet.
        user = self.users.find_one(username=username)
        if user['total_money'] < bet_amount:
            self._post('too_much', comment_info)
        elif bet_table.find_one(user = user['username']) != None:
            self._post('already_bet', comment_info)
        else:
            bet_table.insert(dict(user = user['username'],
                                  bet_amount = round(bet_amount, 2),
                                  bet_string = bet_string,
                                  time_of_bet = float(time),
                                  bet_thread_id = thread['bet_thread_id']))
            new_total = user['total_money'] - bet_amount
            data = dict(id = user['id'], total_money = new_total)
            self.users.update(data, ['id'])
            self._post('bet_made', comment_info)

#-------------------------------------------------------------------------------

    def _getResults(self):

        '''
            pre: none
            post: pulls all closed threads and calculates the winners from
                  each thread, calls _post to make a results thread,
                  updates users table with new values
                  drops betting table for thread
        '''

        closed_threads = self.threads.find(is_thread_open = False)
        for thread in closed_threads:
            if (thread['time_closed'] + 21600) < time.time():
                submission = self.r.get_submission(submission_id = thread['bet_thread_id'])
                comments = submission.comments

                # Find the winners and calculate their winnings
                winners = self._findWinners(thread['bet_thread_id'], thread['pull_thread_id'])
                winner_table = winners[0]
                winner_table = self._calcuateWinnings(winner_table)

                # Caculates the percentages of how many people bet on a comment
                percentages = self._calculateWinnerPercentages(winner_table, len(comments))

                winner_strings = self._createWinnerStrings(winner_table)

                winner_info = [winner_strings, thread['pull_thread_id'],
                               thread['bet_thread_id'], winners[1], percentages]
                results_thread = self._post('results', winner_info)
                results_thread.set_flair(flair_css_class = 'results',
                                        flair_text = 'Results')
                self._messageWinners(winner_table, results_thread)
                # deletes the row in self.threads for the table
                self.threads.delete(id=thread['id'])
                # Cleans up the database of the tables and rows used for the
                # now dead thread
                self._cleanUp(winner_table, thread['bet_thread_id'])
#-------------------------------------------------------------------------------

    def _findWinners(self, thread_table_name, pull_thread_id):

        '''
            pre: thread table name to calculate winners
            post: returns a dictionary of winners
        '''

        thread_table = self.db.get_table(thread_table_name)
        pull_thread = self.r.get_submission(submission_id = pull_thread_id,
                                            comment_limit = 6,
                                            comment_sort = 'top')
        # For some reason, instead of returning 5 comments, praw returns 4
        # and a MoreComments object, this gets rid of it, and prunes the
        # list to only the top 5.
        top_five_comments = pull_thread.replace_more_comments()
        top_five_comments = pull_thread.comments[0:5]

        winner_table = self._createTable('winners')
        winner_columns = {'username': sqlalchemy.String,
                           'bet_amount': sqlalchemy.Float,
                           'bet_string': sqlalchemy.String,
                           'thread_id': sqlalchemy.String,
                           'comment_time': sqlalchemy.Float,
                           'place':  sqlalchemy.Integer,
                           'amount_won': sqlalchemy.Float}

        self._createColumns(winner_table , winner_columns)

        # check users bets
        # if the users bet string is contained in the comment and that comment exists
        # add them to the winner table
        for user in thread_table:
            for top_comment in top_five_comments:
                if self._similarity(user['bet_string'], str(top_comment)) >= .3:
                    if winner_table.find_one(username = user['user']) == None:
                        winner_table.insert(dict(username = user['user'],
                                                 bet_amount = round(user['bet_amount'], 2),
                                                 bet_string = user['bet_string'],
                                                 thread_id = thread_table_name,
                                                 comment_time = user['time_of_bet'],
                                                 place = (top_five_comments.index(top_comment) + 1)))
                    break

        return [winner_table, top_five_comments]

#-------------------------------------------------------------------------------

    def _similarity(self, user_bet, top_comment):

        '''
            pre: a string of the user bet, and the top comment
            post: returns the ratio of how similar the strings are
        '''

        return SequenceMatcher(None, user_bet, top_comment).ratio()

#-------------------------------------------------------------------------------

    def _calcuateWinnings(self, winner_table):

        '''
            pre: winners table
            post: winnings for each winner are added to their account

        '''

        for user in winner_table:
            # current time subtracted by comment time, divided by 12 hours
            time_divisor = abs((time.time() - user['comment_time']) / 43200)
            amount_won_on_bet = round(((user['bet_amount'] * (6 - user['place'])) / time_divisor), 2)

            # update the amount won on the winner table
            data = dict(id = user['id'], amount_won = amount_won_on_bet)
            winner_table.update(data,['id'])

            # update the total money on user table
            user_total = self.users.find_one(username = user['username'])
            new_total = round(user_total['total_money'] + amount_won_on_bet, 2)
            new_data = dict(id = user_total['id'], total_money = new_total)
            self.users.update(new_data, ['id'])

        return winner_table

#-------------------------------------------------------------------------------

    def _createWinnerStrings(self, winner_table):

        '''
            pre: winners table
            post: list of winners strings
        '''

        winner_strings = []

        # runs through each place, and creates a string for each, places each
        # string in winner_string list.
        for place in range(1, 6):
            place_winners = winner_table.find(place = place)
            place_string = ""
            for user in place_winners:
                place_string += ('|{}|{}|{}|\n'.format(user['username'],
                                                      user['amount_won'],
                                                      user['bet_string']))
            winner_strings.append(place_string)

        return winner_strings
#-------------------------------------------------------------------------------

    def _messageWinners(self, winner_table, results_thread):

        '''
            pre: winners table, and the results thread
            post: class _sendMessage() to send the winners a message
        '''

        for user in winner_table:
            self._sendMessage('winner', [user['username'], user['amount_won'],
                              str(results_thread.title), str(results_thread.id)])

#-------------------------------------------------------------------------------

    def _calculateWinnerPercentages(self, winner_table, comment_count):
        '''
            pre: winners table and the amount of comments on post
            post: a list of percentage of that comment bet on in order of place
        '''

        amount_in_place = [0, 0, 0, 0, 0]
        for user in winner_table:
            amount_in_place[user['place'] - 1] += 1

        percentages = [0, 0, 0, 0, 0]
        index = 0
        for place in amount_in_place:
            if place != 0:
                percentages[index] = (place / comment_count) * 100
            else:
                percentages[index] = 0
            index += 1

        return percentages

#-------------------------------------------------------------------------------

    def _cleanUp(self, winner_table, thread_table_name):

        '''
            pre: a winner table, and a bet thread table name
            post: both tables are dropped
        '''

        print "Cleaning up the tables..."
        winner_table.drop()
        self.db[thread_table_name].drop()
        odds_table_name = "odds_" + thread_table_name
        self.db[odds_table_name].drop()

#-------------------------------------------------------------------------------

    def _checkForBetBotComments(self):

        '''
            pre: none
            post: scrapes for Bank comments and replies to them
        '''

        all_comments = self.r.get_comments(self.bet_sub)
        for comment in all_comments:
            comment_info = self._parseObject('comment', comment)
            submission = comment.submission

            # Passes if the comment it is looking at is the sticky
            if submission.id == '37mz6a':
                pass
            # then checks if it has already replied
            elif not self._checkIfBotAlreadyReplied(comment):
                # Checks to see if they registered
                if self._checkIfRegistered(str(comment.author)):
                    if re.search("BetBot! Bank", str(comment), re.IGNORECASE):
                        self._post('bank', comment)
                    elif (re.search("BetBot! Bet", str(comment), re.IGNORECASE) and #checks for Bet comment
                             self.threads.find_one(bet_thread_id = submission.id) != None): #thread is for betting
                        # Checks if it is currently open or closed
                        if self.threads.find_one(bet_thread_id = submission.id)['is_thread_open'] == True:
                            bet_list = str(comment).split(" ")
                            self._makeBet(comment, comment_info, bet_list[2:], time.time(), submission)
                        else:
                            self._post('thread_closed', comment)
                    # Tell the user it has no idea what the want it to do.
                    elif re.search("BetBot!", str(comment), re.IGNORECASE):
                        if self.no_command_replied.find_one(comment_id = str(comment.id)) == None:
                            self._post('no_command', comment)
                # If they haven't, tell them to get registered
                else:
                    self._post('get_registered', comment)

#-------------------------------------------------------------------------------

    def _calculateOdds(self):

        '''
            pre: none
            post: updates Open threads with current leaders, how much the score
                  has changed, and when it was updated
        '''
        
        open_threads = self.threads.find(is_thread_open = True)
        for thread in open_threads:
            pull_thread = self.r.get_submission(submission_id = thread['pull_thread_id'],
                                                comment_limit = 5,
                                                comment_sort = 'top')
            print "On " + pull_thread.title + "..."
            top_five_comments = pull_thread.replace_more_comments()
            top_five_comments = pull_thread.comments[0:5]
            odds_table_name = "odds_" + thread['bet_thread_id']
            odds_table = self.db.get_table(odds_table_name)

            self._createColumns(odds_table, self.odds_table_columns)

            place = 1
            for comment in top_five_comments:
                if odds_table.find_one(comment_id = comment.id) != None:
                    comment_row = odds_table.find_one(comment_id = comment.id)
                    # If the current comment is in the same place as last time
                    # Just update it
                    if comment_row['id'] == place:
                        data = dict(id=place, score = comment.score,
                                    delta = comment.score - comment_row['score'],
                                    updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                        odds_table.update(data, ['id'])
                    else:
                        old_comment = odds_table.find_one(id = 6)
                        # If the current comment used to be in the top 5
                        # Bring it back up into view, and put the one it replaces
                        # into ID = 6 in case we need to keep the delta
                        if old_comment != None and comment.id == old_comment['comment_id']:
                            replaced_comment = odds_table.find_one(id = place)
                            data = dict(id=place, comment = str(comment),
                                        comment_id = comment.id, score = comment.score,
                                        delta = comment.score - old_comment['score'],
                                        updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                            odds_table.update(data, ['id'])

                            data = dict(id = 6, comment = old_comment['comment'],
                                        comment_id = old_comment['comment_id'],
                                        score = old_comment['score'],
                                        delta = old_coment['delta'],
                                        updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                            odds_table.update(data, ['id'])
                        # If current comment is replacing a comment currently visible
                        # comment store the one its replacing and update with a new one
                        else:
                            replaced_comment = odds_table.find_one(id = place)
                            replacing_comment = odds_table.find_one(comment_id = comment.id)

                            data = dict(id=place, comment = str(comment),
                                        comment_id = comment.id,
                                        score = comment.score,
                                        delta = comment.score - replacing_comment['score'],
                                        updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                            odds_table.update(data, ['id'])

                            data = dict(id = 6, comment = replaced_comment['comment'],
                                        comment_id = replaced_comment['comment_id'],
                                        score = replaced_comment['score'],
                                        delta = replaced_comment['delta'],
                                        updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                            odds_table.update(data, ['id'])
                # If the entry doesn't exist, create it
                else:
                    data = dict(id=place, comment = str(comment),
                                comment_id = comment.id, score = comment.score,
                                delta = 0,
                                updated = (time.strftime('%H:%M', time.gmtime()) + " UTC"))
                    odds_table.upsert(data, ['id'])

                place += 1

            d = {'bet_thread': thread['bet_thread_id'],
                 'title': thread['thread_title'],
                 'pull_id': thread['pull_thread_id'],
                 'table': odds_table }
            self._post("odds", d)

#-------------------------------------------------------------------------------

    def _checkIfBotAlreadyReplied(self, comment):

        '''
            pre: a comment object
            post: returns a boolean, true if bot as already replied, False
                  if not.
        '''

        if (self.replied_to.find_one(comment_id = str(comment.id)) != None):
            return True
        else:
            return False


#-------------------------------------------------------------------------------

    def _checkIfRegistered(self, username):

        '''
            pre: username for the person to check
            post: returns True if they are registered, False if not.
        '''
        if self.users.find_one(username = username) == None:
            return False
        else:
            return True

#-------------------------------------------------------------------------------

    def _checkIfUserIsBroke(self):

        '''
            pre: none
            post: sends a message to any users who go below 1 point
        '''

        broke_users = self.db.query('SELECT total_money, username FROM users WHERE total_money < 1')
        for row in broke_users:
            user = self.users.find_one(username = row['username'])
            data = dict(id = user['id'], total_money = 1000)
            self.users.update(data, ['id'])

            self._sendMessage('broke', user['username'])

#-------------------------------------------------------------------------------

    def _post(self, type_of_post, info):

        '''
            pre: type-string, info-list or dic depending of type
            post: a thread is posted of the corresponding type in the
                  bet sub. Returns parsed submission object.
        '''

        #-----------------------------------------------------------------------

        def openBet(info):
            bet_title = OPEN_POST_TITLE.format(info[0])
            bet_body = OPEN_THREAD_BODY.format(info[0], info[1])

            print "Bot posting new open thread..."
            if self.debug == True:
                print bet_title + '\n' + bet_body
                return info
            else:
                bet_post = self.r.submit(self.bet_sub, bet_title, text = bet_body)
                return self._parseObject('submission', bet_post)

        #-----------------------------------------------------------------------

        def results(info):
            submission_title = str(self.r.get_submission(submission_id = info[1]).title)
            winners_strings = info[0]
            top_comments = info[3]
            percentages = info[4]
            results_title = RESULTS_POST_TITLE.format(submission_title)
            results_body = RESULTS_THREAD_BODY.format(submission_title,
                                                      str(top_comments[0]).replace('\n', ' '),
                                                      top_comments[0].score,
                                                      percentages[0],
                                                      winners_strings[0],
                                                      str(top_comments[1]).replace('\n', ' '),
                                                      top_comments[1].score,
                                                      percentages[1],
                                                      winners_strings[1],
                                                      str(top_comments[2]).replace('\n', ' '),
                                                      top_comments[2].score,
                                                      percentages[2],
                                                      winners_strings[2],
                                                      str(top_comments[3]).replace('\n', ' '),
                                                      top_comments[3].score,
                                                      percentages[3],
                                                      winners_strings[3],
                                                      str(top_comments[4]).replace('\n', ' '),
                                                      top_comments[4].score,
                                                      percentages[4],
                                                      winners_strings[4],
                                                      info[1],
                                                      info[2])

            print "Bot posting new results thread..."
            if self.debug == True:
                print results_title + '\n' + results_body
                return info
            else:
                results_post = self.r.submit(self.bet_sub, results_title, text = results_body)
                return results_post

        #-----------------------------------------------------------------------

        def registered(info):
            msg = REGISTERED_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.replied_to.insert(dict(comment_id = str(info.id),
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def get_registered(info):
            comment = info
            msg = GET_REGISTERED_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = comment.id,
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def too_much(info):
            comment = info[-1]
            user= self.users.find_one(username=info[0])
            user_money = user['total_money']
            msg = TOO_MUCH_REPLY.format(user_money)

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def bet_made(info):
            comment = info[-1]
            msg = BET_MADE_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def already_bet(info):
            comment = info[-1]
            msg = BET_TWICE_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def bank(info):
            user = self.users.find_one(username = str(info.author))
            msg = BANK_REPLY.format(user['total_money'])

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.replied_to.insert(dict(comment_id = info.id,
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def error(info):
            if self.no_command_replied.find_one(comment_id = str(comment.id)) == None:
                if self.debug == True:
                    pass
                else:
                    print "Replying to user..."
                    info.reply("Sorry, but you didn't put a number there, edit it to add one!")
                    self.no_command_replied.insert(dict(comment_id = info.id,
                                                        date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def no_command(info):
            msg = FORGOT_COMMAND_REPLY
            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.no_command_replied.insert(dict(comment_id = info.id,
                                                    date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def thread_closed(info):
            msg = CLOSED_THREAD_REPLY
            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.replied_to.insert(dict(comment_id = info.id,
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def odds(info):
            thread = self.r.get_submission(submission_id = info['bet_thread'])
            odds_table = info['table']
            first = odds_table.find_one(id = 1)
            second = odds_table.find_one(id = 2)
            third = odds_table.find_one(id = 3)
            fourth = odds_table.find_one(id = 4)
            fifth = odds_table.find_one(id = 5)

            body = OPEN_THREAD_BODY.format(info['title'], info['pull_id'])
            thread_table = OPEN_THREAD_ODDS.format(first['id'],
                                                   first['comment'].replace('\n', ' '),
                                                   first['score'],
                                                   first['delta'],
                                                   first['updated'],
                                                   second['id'],
                                                   second['comment'].replace('\n', ' '),
                                                   second['score'],
                                                   second['delta'],
                                                   second['updated'],
                                                   third['id'],
                                                   third['comment'].replace('\n', ' '),
                                                   third['score'],
                                                   third['delta'],
                                                   third['updated'],
                                                   fourth['id'],
                                                   fourth['comment'].replace('\n', ' '),
                                                   fourth['score'],
                                                   fourth['delta'],
                                                   fourth['updated'],
                                                   fifth['id'],
                                                   fifth['comment'].replace('\n', ' '),
                                                   fifth['score'],
                                                   fifth['delta'],
                                                   fifth['updated'])
            print "Editing Thread..."
            thread.edit(body + thread_table)

        #-----------------------------------------------------------------------

        options = { 'open': openBet,
                    'results': results,
                    'registered': registered,
                    'get_registered': get_registered,
                    'too_much': too_much,
                    'bet_made': bet_made,
                    'already_bet': already_bet,
                    'bank': bank,
                    'error': error,
                    'no_command': no_command,
                    'thread_closed': thread_closed,
                    'odds': odds
        }

        return options[type_of_post](info)

#-------------------------------------------------------------------------------

    def _parseObject(self, obj_type, obj):

        '''
            pre: obj - a praw reddit obj to be converted
            post: returns a list of the relevant information converted to
                  python types.
        '''

        #-----------------------------------------------------------------------

        def comment(obj):
            return [str(obj.author), str(obj.id)]

        #-----------------------------------------------------------------------

        def submission(obj):
            return [str(obj.title), str(obj.id)]

        #-----------------------------------------------------------------------

        def user(obj):
            pass

        #-----------------------------------------------------------------------

        def subreddit(obj):
            pass

        #-----------------------------------------------------------------------

        options = { 'comment': comment,
                    'submission': submission,
                    'user': user,
                    'subreddit': subreddit
        }

        return options[obj_type](obj)
#-------------------------------------------------------------------------------

    def _sendMessage(self, group, info):

        '''
            pre: type-type of message to send, users-list of users to message
            post: PMs are sent to each users
        '''

        #-----------------------------------------------------------------------

        def winner(info):
            sub = "Congrats you won!"
            msg = WINNER_MESSAGE.format(info[2],
                                        info[1],
                                        info[3])
            if self.debug == True:
                print msg
            else:
                self.r.send_message(info[0], sub, msg, captcha=None)

        #-----------------------------------------------------------------------

        def broke(info):
            sub = "You're broke buddy."
            msg = "Saw you were low on cash, so I gave you a free 1000."

            if self.debug == True:
                print msg
            else:
                self.r.send_message(info, sub, msg, captcha = None)

        #-----------------------------------------------------------------------

        options = { 'winner': winner,
                    'broke': broke }

        options[group](info)

#-------------------------------------------------------------------------------

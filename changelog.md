Changelog
=========

6/20/2015
---------

* Fixed when it calculates percentages for results threads
  * It was subtracting instead of dividing....

6/17/2015
---------

* Open threads now display the current top 5 comments of the thread.
  * Displays the comment score
  * Updated every 30 minutes, the change of score is displayed
* All AskReddit threads that are linked in this sub are linked in
  No Participation mode now.

6/16/2015
---------

* Edited the results thread formatting to use tables.
* Better commenting

6/15/2015
---------

* Big changes to results threads
  * Now posts the top 5 comments from the thread
  * Posts the percentages of people that have commented
      * Not sure if it actually works because not enough people are commenting. :(
* Some bug fixes

6/14/2015
---------

* Started keeping a Changelog
* Hopefully fixed the bug where a users money gets set to 0
  * Not sure what caused it, and it appeared to only happen to a 2 users.
* Results threads now include links to the corresponding AskReddit and BetOnAskReddit threads.
* Cleaned up the awful mess that the checkForBetBotComments method was.

import dataset
import praw
from praw.handlers import MultiprocessHandler
from BetBot import BetBot
from config_bot import *

db = dataset.connect('sqlite:///ar_bot.db')

user_agent = (REDDIT_USERAGENT)
handler = MultiprocessHandler()
r = praw.Reddit(user_agent = user_agent, handler = handler)

odds = BetBot(db,
             r,
             "betonaskreddit",
             "askreddit",
             REDDIT_USERNAME,
             REDDIT_PASS,
             REDDIT_USERAGENT,
             False)

odds.start(job = 'odds')
